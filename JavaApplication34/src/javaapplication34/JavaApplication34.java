
package javaapplication34;
import java.util.Scanner; 
public class JavaApplication34 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //variable 
        int num= 0; 
        //Pedir número
        Scanner sc=new Scanner (System.in);
        //opciones del menu
        System.out.println("1.Información general");
        System.out.println("2.Efectos/Síntomas");
        System.out.println("3.Población vunerable");
        System.out.println("4.Estados infectados");
        System.out.println("5.Medidas de higiene");
        System.out.println("Ingrese un número del 1 al 5 para desplehar la información que requiere");
        num = sc.nextInt();
        //información que sale dependiendo del número
        if (num==1){
            System.out.println("El nuevo coronavirus Covid-19 tuvo su origen en la ciudad de Wuhan,en China. ");
            System.out.println("A mediados del mes de diciembre de 2019, las autoridades sanitarias de Wuhan detectaron");
            System.out.println("una serie de casos de neumonía producida por una causa desconocida.");
        }else if (num==2) {
            System.out.println("En general, los síntomas principales de las infecciones por coronavirus pueden ser los siguientes:");
            System.out.println("tos");
            System.out.println("dolor de garganta");
            System.out.println("fiebre");
            System.out.println("dificultad de respirar");
            System.out.println("dolor de cabeza");
            System.out.println("escalofríos");
            System.out.println("secreción y goteo nasal");
        }else if (num==3){
            System.out.println("El Covid-19 afecta a las personas mayores y las que padecen afecciones médicas preexistentes como: hipertensión arterial, obesidad, diabetes, o patología cardiovascular y cerebrovascular desarrollan casos graves de la enfermedad con más frecuencia que otras. ");
        }else if(num==4){
            System.out.println("Campeche: 2");
            System.out.println("Yucatán: 13");
            System.out.println("Quintana Roo: 6");
        }else if (num==5){
            System.out.println("Hasta la fecha no se dispone de vacuna alguna ni de tratamiento específico para combatir la infección por coronavirus.");
            System.out.println("Mantener una higiene básica es la forma más eficaz de evitar contraer este virus en los lugares en los que existe un mayor riesgo de transmisión, fundamentalmente las zonas en las que se han registrado casos. Es conveniente lavarse las manos con frecuencia y evitar el contacto con personas ya infectadas, protegiendo especialmente ojos, nariz y boca. A las personas infectadas (o que crean que pueden estarlo) se les aconseja el uso de mascarillas y usar pañuelos para cubrirse la nariz y la boca cuando se tose o se estornuda.");
            System.out.println("Las personas infectadas por el virus que causa el COVID-19 deben guardar cuarentena desde el diagnóstico de la enfermedad hasta 15 días después de ser dadas de alta. Así lo aconseja la OMS porque se ha observado que, aunque ya estén recuperadas, pueden seguir transmitiendo la infección.");
        }
     
        
    }
    
}
